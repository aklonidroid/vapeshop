package tabacco.shop.vape.vapeshop.listeners;

import java.util.List;

import tabacco.shop.vape.vapeshop.data.ingredients.title.ItemIngredientInterface;

public interface IngredientsListener {
    void onIngredientsClick(List<ItemIngredientInterface> ingredients);
    int getCountByIngredients(List<ItemIngredientInterface> ingredients);
}
