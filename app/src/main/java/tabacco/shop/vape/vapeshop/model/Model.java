package tabacco.shop.vape.vapeshop.model;

import android.app.Activity;
import android.support.annotation.NonNull;

import java.net.MalformedURLException;
import java.net.URL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tabacco.shop.vape.vapeshop.DataRest;
import tabacco.shop.vape.vapeshop.data.DataObject;
import tabacco.shop.vape.vapeshop.listeners.CallbackView;

public class Model {
    private CallbackView view;

    public Model(CallbackView view) {
        if(view == null)
            throw new IllegalArgumentException();
        this.view = view;
    }

    //делаем запрос к сети
    public void getAllData(String urlFull) {
        URL url = null;
        try {
            url = new URL(urlFull);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("https://google.com/")
                .baseUrl("http://" + url.getAuthority() + "/")
                .addConverterFactory(GsonConverterFactory.create()) //Конвертер, необходимый для преобразования JSON'а в объекты
                .build();

        DataRest dataRest = retrofit.create(DataRest.class);
        dataRest.getAllData(url.getFile()).enqueue(new Callback<DataObject>() {
//        dataRest.getAllData("").enqueue(new Callback<DataObject>() {

            @Override
            public void onResponse(@NonNull Call<DataObject> call, @NonNull Response<DataObject> response) {
                //Данные успешно пришли, но надо проверить response.body() на null
                if(!((Activity)view).isDestroyed() && response.body() != null) {
                    view.updateAllData(response.body());
                } else {
                    view.onFailure();
                }
            }
            @Override
            public void onFailure(@NonNull Call<DataObject> call, @NonNull Throwable t) {
                if(!((Activity)view).isDestroyed()) {
                    view.onFailure();
                }
            }
        });
    }

}
