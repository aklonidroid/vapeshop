package tabacco.shop.vape.vapeshop.adapter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import tabacco.shop.vape.vapeshop.R;
import tabacco.shop.vape.vapeshop.data.Category;
import tabacco.shop.vape.vapeshop.listeners.MainActivityListener;


public class MainFragmentAdapter extends RecyclerView.Adapter<MainFragmentAdapter.MyViewHolder> {
    private final int mItemLayout;
    private List<Category> mCategoryList;
    private MainActivityListener mActivityListener;

    public MainFragmentAdapter(List<Category> mCategoryList, int mItemLayout) {
        this.mCategoryList = mCategoryList;
        this.mItemLayout = mItemLayout;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(mItemLayout, null, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        Category category = mCategoryList.get(position);
        holder.mTextView.setText(category.getCat_name());
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions.transforms(new CenterCrop(), new RoundedCorners(90));
        Glide.with(holder.mImageView)
                .load(category.getCat_image())
                .apply(requestOptions)
                .into(holder.mImageView);

        holder.mCategoryContainer.setOnClickListener(view -> mActivityListener.onCategoryButtonClick(category.getCategoryId()));
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mTextView;
        ImageView mImageView;
        RelativeLayout mCategoryContainer;

        MyViewHolder(View view) {
            super(view);
            mTextView = view.findViewById(R.id.category_name);
            mImageView = view.findViewById(R.id.image_category);
            mCategoryContainer = view.findViewById(R.id.category_container);
        }
    }

    public void setOnActivityLister(MainActivityListener mActivityListener) {
        this.mActivityListener = mActivityListener;
    }

}
