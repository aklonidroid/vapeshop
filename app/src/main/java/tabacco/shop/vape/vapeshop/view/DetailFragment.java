package tabacco.shop.vape.vapeshop.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Objects;

import tabacco.shop.vape.vapeshop.R;
import tabacco.shop.vape.vapeshop.data.VapeSwill;
import tabacco.shop.vape.vapeshop.data.ingredients.Ingredient;
import tabacco.shop.vape.vapeshop.utils.Utils;

public class DetailFragment extends BaseFragment implements View.OnClickListener {
    private VapeSwill vapeSwill;
    private TextView mItemName;
    private TextView mItemPrice;
    private TextView mItemDescription;
    private ImageView mVapeSwillImage;
    private ImageView mIngredientImage1;
    private TextView mIngredientTitle1;
    private ImageView mIngredientImage2;
    private TextView mIngredientTitle2;
    private ImageView mIngredientImage3;
    private TextView mIngredientTitle3;
    private ImageView mIngredientImage4;
    private TextView mIngredientTitle4;
    private ImageView mIngredientImage5;
    private TextView mIngredientTitle5;
    private ImageView mIngredientImage6;
    private TextView mIngredientTitle6;

    @Override
    public int getLayout() {
        return R.layout.fragment_detail;
    }

    @Override
    public void createContent(View view) {
        mItemName = view.findViewById(R.id.item_name);
        mItemPrice = view.findViewById(R.id.item_price);
        mItemDescription = view.findViewById(R.id.item_description);
        mVapeSwillImage = view.findViewById(R.id.image_vape_swill_detail);

        mIngredientTitle1 = view.findViewById(R.id.first_flavor_title);
        mIngredientImage1 = view.findViewById(R.id.first_flavor_image);
        mIngredientImage1.setOnClickListener(this);

        mIngredientTitle2 = view.findViewById(R.id.second_flavor_title);
        mIngredientImage2 = view.findViewById(R.id.second_flavor_image);
        mIngredientImage2.setOnClickListener(this);

        mIngredientTitle3 = view.findViewById(R.id.third_flavor_title);
        mIngredientImage3 = view.findViewById(R.id.third_flavor_image);
        mIngredientImage3.setOnClickListener(this);

        mIngredientTitle4 = view.findViewById(R.id.fourth_flavor_title);
        mIngredientImage4 = view.findViewById(R.id.fourth_flavor_image);
        mIngredientImage4.setOnClickListener(this);

        mIngredientTitle5 = view.findViewById(R.id.fifth_flavor_title);
        mIngredientImage5 = view.findViewById(R.id.fifth_flavor_image);
        mIngredientImage5.setOnClickListener(this);

        mIngredientTitle6 = view.findViewById(R.id.sixth_flavor_title);
        mIngredientImage6 = view.findViewById(R.id.sixth_flavor_image);
        mIngredientImage6.setOnClickListener(this);

        setDataUI();
        view.findViewById(R.id.button_close).setOnClickListener(view1 -> {
            hideDetailFragment();
        });
    }

    private void hideDetailFragment() {
        ((MainActivity) Objects.requireNonNull(getActivity())).showIngredientsButton();
        getFragmentManager().beginTransaction().hide(this).commit();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) {
            setDataUI();
        }
        super.onHiddenChanged(hidden);
    }

    @Override
    public void onClick(View view) {
        Ingredient flavor = null;
        switch (view.getId()) {
            case R.id.first_flavor_image:
                flavor = (Ingredient) mIngredientTitle1.getTag();
                break;
            case R.id.second_flavor_image:
                flavor = (Ingredient) mIngredientTitle2.getTag();
                break;
            case R.id.third_flavor_image:
                flavor = (Ingredient) mIngredientTitle3.getTag();
                break;
            case R.id.fourth_flavor_image:
                flavor = (Ingredient) mIngredientTitle4.getTag();
                break;
            case R.id.fifth_flavor_image:
                flavor = (Ingredient) mIngredientTitle5.getTag();
                break;
            case R.id.sixth_flavor_image:
                flavor = (Ingredient) mIngredientTitle6.getTag();
                break;
        }
        ((MainActivity) getActivity()).setIngredientFromDetailFragment(flavor);
        hideDetailFragment();
    }

    private void setDataUI() {
        Glide.with(this)
                .load(vapeSwill.getPicture())
                .into(mVapeSwillImage);

        mItemName.setText(vapeSwill.getName());
        Utils.setStockPrice(mItemPrice, vapeSwill.getPrice(), vapeSwill.getOldprice());
        mItemDescription.setText(vapeSwill.getDescription());

        List<Ingredient> flavors = Utils.getAttributeList(vapeSwill.getAttributes());

        hideShow(flavors.size());
        for (int i = 0; i < flavors.size(); i++) {
            Ingredient ingredient = flavors.get(i);
            switch (i) {
                case 0:
                    loadImageAndSetTitle(mIngredientImage1, mIngredientTitle1, ingredient);
                    break;
                case 1:
                    loadImageAndSetTitle(mIngredientImage2, mIngredientTitle2, ingredient);
                    break;
                case 2:
                    loadImageAndSetTitle(mIngredientImage3, mIngredientTitle3, ingredient);
                    break;
                case 3:
                    loadImageAndSetTitle(mIngredientImage4, mIngredientTitle4, ingredient);
                    break;
                case 4:
                    loadImageAndSetTitle(mIngredientImage5, mIngredientTitle5, ingredient);
                    break;
                case 5:
                    loadImageAndSetTitle(mIngredientImage6, mIngredientTitle6, ingredient);
                    break;
            }
        }
    }

    private void loadImageAndSetTitle(ImageView imageView, TextView textView, Ingredient ingredient) {
        textView.setText(ingredient.getName());
        textView.setTag(ingredient);
        Glide.with(imageView)
                .load(ingredient.getUrl())
                .into(imageView);
    }

    private void hideShow(int size) {
        mIngredientTitle1.setVisibility(size >= 1 ? View.VISIBLE : View.GONE);
        mIngredientImage1.setVisibility(size >= 1 ? View.VISIBLE : View.GONE);
        mIngredientTitle2.setVisibility(size >= 2 ? View.VISIBLE : View.GONE);
        mIngredientImage2.setVisibility(size >= 2 ? View.VISIBLE : View.GONE);
        mIngredientTitle3.setVisibility(size >= 3 ? View.VISIBLE : View.GONE);
        mIngredientImage3.setVisibility(size >= 3 ? View.VISIBLE : View.GONE);
        mIngredientTitle4.setVisibility(size >= 4 ? View.VISIBLE : View.GONE);
        mIngredientImage4.setVisibility(size >= 4 ? View.VISIBLE : View.GONE);
        mIngredientTitle5.setVisibility(size >= 5 ? View.VISIBLE : View.GONE);
        mIngredientImage5.setVisibility(size >= 5 ? View.VISIBLE : View.GONE);
        mIngredientTitle6.setVisibility(size >= 6 ? View.VISIBLE : View.GONE);
        mIngredientImage6.setVisibility(size >= 6 ? View.VISIBLE : View.GONE);
    }

    public void setVapeSwillData(VapeSwill item) {
        vapeSwill = item;
    }
}
