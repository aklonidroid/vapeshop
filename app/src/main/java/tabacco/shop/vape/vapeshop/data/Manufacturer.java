package tabacco.shop.vape.vapeshop.data;

import java.io.Serializable;

public class Manufacturer implements Serializable {
    private int manufacturerId;
    private String man_name;
    private String man_image;

    public int getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    public String getMan_name() {
        return man_name;
    }

    public void setMan_name(String man_name) {
        this.man_name = man_name;
    }

    public String getMan_image() {
        return man_image;
    }

    public void setMan_image(String man_image) {
        this.man_image = man_image;
    }
}
