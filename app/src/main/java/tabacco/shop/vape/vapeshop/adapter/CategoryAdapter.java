package tabacco.shop.vape.vapeshop.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import tabacco.shop.vape.vapeshop.R;
import tabacco.shop.vape.vapeshop.data.VapeSwill;
import tabacco.shop.vape.vapeshop.data.ingredients.Ingredient;
import tabacco.shop.vape.vapeshop.listeners.MainActivityListener;
import tabacco.shop.vape.vapeshop.utils.Utils;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryHolder> {
    private final int mItemLayout;
    private List<VapeSwill> mCategoryList;
    private MainActivityListener mActivityListener;

    public CategoryAdapter(List<VapeSwill> mCategoryList, int mItemLayout) {
        this.mCategoryList = mCategoryList;
        this.mItemLayout = mItemLayout;
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryHolder(LayoutInflater.from(parent.getContext()).inflate(mItemLayout, null, false));
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHolder holder, final int position) {
        VapeSwill vapeSwill = mCategoryList.get(position);

        holder.mTitle.setText(vapeSwill.getName());
        Glide.with(holder.mVapeSwillImage)
                .load(vapeSwill.getPicture())
                .into(holder.mVapeSwillImage);
        Utils.setStockPrice(holder.mPrice, vapeSwill.getPrice(), vapeSwill.getOldprice());
        holder.mCardView.setOnClickListener(view -> mActivityListener.onDetailClick(vapeSwill));

        List<Ingredient> ingredients = Utils.getAttributeList(vapeSwill.getAttributes());
        hideShowCardViews(holder, ingredients.size());

        for (int i = 0; i < ingredients.size(); i++) {
            String text = ingredients.get(i).getName();
            String url = ingredients.get(i).getUrl();

            float size;
            float scaleRatio = holder.mVapeSwillImage.getResources().getDisplayMetrics().density;
            //если текст не помещается в строку, то уменьшаем размер
            if (text.length() > 7 && text.length() < 9) {
                size = holder.mVapeSwillImage.getResources().getDimension(R.dimen.text_category_ingredients_size_small) / scaleRatio;
            } else {
                size = holder.mVapeSwillImage.getResources().getDimension(R.dimen.text_category_ingredients_size) / scaleRatio;
            }
            switch (i) {
                case 0:
                    setImageAndTitle(holder.mIngredientImage1, holder.mIngredientTitle1, url, size, text);
                    break;
                case 1:
                    setImageAndTitle(holder.mIngredientImage2, holder.mIngredientTitle2, url, size, text);
                    break;
                case 2:
                    setImageAndTitle(holder.mIngredientImage3, holder.mIngredientTitle3, url, size, text);
                    break;
                case 3:
                    setImageAndTitle(holder.mIngredientImage4, holder.mIngredientTitle4, url, size, text);
                    break;
                case 4:
                    setImageAndTitle(holder.mIngredientImage5, holder.mIngredientTitle5, url, size, text);
                    break;
                case 5:
                    setImageAndTitle(holder.mIngredientImage6, holder.mIngredientTitle6, url, size, text);
                    break;
            }
        }
    }

    //Скрываем элемент, если к нему нет ингридиента
    private void hideShowCardViews(CategoryHolder holder, int size) {
        holder.mCardView1.setVisibility(size >= 1 ? View.VISIBLE : View.GONE);
        holder.mCardView2.setVisibility(size >= 2 ? View.VISIBLE : View.GONE);
        holder.mCardView3.setVisibility(size >= 3 ? View.VISIBLE : View.GONE);
        holder.mCardView4.setVisibility(size >= 4 ? View.VISIBLE : View.GONE);
        holder.mCardView5.setVisibility(size >= 5 ? View.VISIBLE : View.GONE);
        holder.mCardView6.setVisibility(size >= 6 ? View.VISIBLE : View.GONE);
        holder.mIngredientTitle1.setVisibility(size >= 1 ? View.VISIBLE : View.GONE);
        holder.mIngredientImage1.setVisibility(size >= 1 ? View.VISIBLE : View.GONE);
        holder.mIngredientTitle2.setVisibility(size >= 2 ? View.VISIBLE : View.GONE);
        holder.mIngredientImage2.setVisibility(size >= 2 ? View.VISIBLE : View.GONE);
        holder.mIngredientTitle3.setVisibility(size >= 3 ? View.VISIBLE : View.GONE);
        holder.mIngredientImage3.setVisibility(size >= 3 ? View.VISIBLE : View.GONE);
        holder.mIngredientTitle4.setVisibility(size >= 4 ? View.VISIBLE : View.GONE);
        holder.mIngredientImage4.setVisibility(size >= 4 ? View.VISIBLE : View.GONE);
        holder.mIngredientTitle5.setVisibility(size >= 5 ? View.VISIBLE : View.GONE);
        holder.mIngredientImage5.setVisibility(size >= 5 ? View.VISIBLE : View.GONE);
        holder.mIngredientTitle6.setVisibility(size >= 6 ? View.VISIBLE : View.GONE);
        holder.mIngredientImage6.setVisibility(size >= 6 ? View.VISIBLE : View.GONE);
    }

    private void setImageAndTitle(ImageView imageView, TextView textView, String url, float size, String text) {
        textView.setTextSize(size);
        textView.setText(text);
        Glide.with(imageView)
                .load(url)
                .into(imageView);
    }

    public void setOnActivityLister(MainActivityListener activityListener) {
        this.mActivityListener = activityListener;
    }

    static class CategoryHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        TextView mPrice;
        CardView mCardView;
        ImageView mVapeSwillImage;
        ImageView mIngredientImage1;
        TextView mIngredientTitle1;
        ImageView mIngredientImage2;
        TextView mIngredientTitle2;
        ImageView mIngredientImage3;
        TextView mIngredientTitle3;
        ImageView mIngredientImage4;
        TextView mIngredientTitle4;
        ImageView mIngredientImage5;
        TextView mIngredientTitle5;
        ImageView mIngredientImage6;
        TextView mIngredientTitle6;
        CardView mCardView1;
        CardView mCardView2;
        CardView mCardView3;
        CardView mCardView4;
        CardView mCardView5;
        CardView mCardView6;

       private CategoryHolder(View view) {
            super(view);
            mTitle = view.findViewById(R.id.title_description);
            mCardView = view.findViewById(R.id.card_view);
            mPrice = view.findViewById(R.id.price);
            mVapeSwillImage = view.findViewById(R.id.image_vape_swill);

            mIngredientTitle1 = view.findViewById(R.id.first_flavor_title);
            mIngredientImage1 = view.findViewById(R.id.first_flavor_image);

            mIngredientTitle2 = view.findViewById(R.id.second_flavor_title);
            mIngredientImage2 = view.findViewById(R.id.second_flavor_image);

            mIngredientTitle3 = view.findViewById(R.id.third_flavor_title);
            mIngredientImage3 = view.findViewById(R.id.third_flavor_image);

            mIngredientTitle4 = view.findViewById(R.id.fourth_flavor_title);
            mIngredientImage4 = view.findViewById(R.id.fourth_flavor_image);

            mIngredientTitle5 = view.findViewById(R.id.fifth_flavor_title);
            mIngredientImage5 = view.findViewById(R.id.fifth_flavor_image);

            mIngredientTitle6 = view.findViewById(R.id.sixth_flavor_title);
            mIngredientImage6 = view.findViewById(R.id.sixth_flavor_image);
            mCardView1 = view.findViewById(R.id.card_view_first);
            mCardView2 = view.findViewById(R.id.card_view_second);
            mCardView3 = view.findViewById(R.id.card_view_third);
            mCardView4 = view.findViewById(R.id.card_view_fourth);
            mCardView5 = view.findViewById(R.id.card_view_fifth);
            mCardView6 = view.findViewById(R.id.card_view_sixth);
        }
    }
}
