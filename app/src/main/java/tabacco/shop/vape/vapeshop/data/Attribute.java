package tabacco.shop.vape.vapeshop.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import tabacco.shop.vape.vapeshop.data.ingredients.Booster;
import tabacco.shop.vape.vapeshop.data.ingredients.Desert;
import tabacco.shop.vape.vapeshop.data.ingredients.Drink;
import tabacco.shop.vape.vapeshop.data.ingredients.Fresh;
import tabacco.shop.vape.vapeshop.data.ingredients.Fruit;
import tabacco.shop.vape.vapeshop.data.ingredients.Tobacco;
import tabacco.shop.vape.vapeshop.data.other.NicotineContent;
import tabacco.shop.vape.vapeshop.data.other.Size;

public class Attribute implements Serializable {
    @SerializedName("Tobacco")
    private List<Tobacco> tobacco;
    @SerializedName("Fruits")
    private List<Fruit> fruits;
    @SerializedName("Drinks")
    private List<Drink> drinks;
    @SerializedName("Deserts")
    private List<Desert> deserts;
    @SerializedName("Booster")
    private List<Booster> booster;
    @SerializedName("Fresh")
    private List<Fresh> freshes;
    @SerializedName("Nicotine_content")
    private List<NicotineContent> nicotineContents;
    @SerializedName("Size")
    private List<Size> sizes;

    public List<Booster> getBooster() {
        return booster;
    }

    public void setBooster(List<Booster> booster) {
        this.booster = booster;
    }

    public List<Tobacco> getTobacco() {
        return tobacco;
    }

    public List<Fruit> getFruits() {
        return fruits;
    }

    public void setFruits(List<Fruit> fruits) {
        this.fruits = fruits;
    }

    public List<Drink> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Drink> drinks) {
        this.drinks = drinks;
    }

    public List<NicotineContent> getNicotineContents() {
        return nicotineContents;
    }

    public void setNicotineContents(List<NicotineContent> nicotineContents) {
        this.nicotineContents = nicotineContents;
    }

    public List<Size> getSizes() {
        return sizes;
    }

    public void setSizes(List<Size> sizes) {
        this.sizes = sizes;
    }

    public void setTobacco(List<Tobacco> tobacco) {
        this.tobacco = tobacco;
    }

    public List<Desert> getDeserts() {
        return deserts;
    }

    public void setDeserts(List<Desert> deserts) {
        this.deserts = deserts;
    }

    public List<Fresh> getFreshes() {
        return freshes;
    }

    public void setFreshes(List<Fresh> freshes) {
        this.freshes = freshes;
    }
}
