package tabacco.shop.vape.vapeshop.view;

import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tabacco.shop.vape.vapeshop.R;
import tabacco.shop.vape.vapeshop.adapter.CategoryAdapter;
import tabacco.shop.vape.vapeshop.data.VapeSwill;
import tabacco.shop.vape.vapeshop.data.ingredients.title.ItemIngredientInterface;
import tabacco.shop.vape.vapeshop.listeners.IngredientsListener;
import tabacco.shop.vape.vapeshop.listeners.MainActivityListener;
import tabacco.shop.vape.vapeshop.utils.Utils;

public class CategoryFragment extends BaseFragment implements IngredientsListener, View.OnClickListener {
    //выбранные элементы исходя из ингридиентов
    private static List<VapeSwill> currentData = new ArrayList<>();
    //все элементы текущей категории
    public static List<VapeSwill> allData = new ArrayList<>();
    private MainActivityListener mActivityListener;

    @Override
    public int getLayout() {
        return R.layout.fragment_category;
    }

    @Override
    public void createContent(View view) {
        mRecyclerView = view.findViewById(R.id.all_products_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        view.findViewById(R.id.sort_down).setOnClickListener(this);
        view.findViewById(R.id.sort_up).setOnClickListener(this);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 4));

        mAdapter = new CategoryAdapter(currentData, R.layout.item_category);
        ((CategoryAdapter) mAdapter).setOnActivityLister(mActivityListener);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        allData.clear();
        currentData.clear();
        mRecyclerView = null;
        mAdapter = null;
    }

    //Данный с выбраными вкусами
    @Override
    public void onIngredientsClick(List<ItemIngredientInterface> ingredients) {
        currentData.clear();
        if (ingredients.size() > 0) {
            for (VapeSwill vapeSwill : allData) {
                boolean isContainAllIngredients = true;
                for (ItemIngredientInterface ingredient : ingredients) {
                    if (!Utils.getIngredientsFromList(vapeSwill.getAttributes()).contains(ingredient) ||
                            currentData.contains(vapeSwill)) {
                        isContainAllIngredients = false;
                        break;
                    }
                }
                if (isContainAllIngredients) {
                    currentData.add(vapeSwill);
                }
            }
        } else {
            currentData.addAll(allData);
        }
        mAdapter.notifyDataSetChanged();
    }
    @Override
    public int getCountByIngredients(List<ItemIngredientInterface> ingredients) {
        if (ingredients.size() == 0) {
            return 0;
        }

        int countIngredients = 0;
        for (VapeSwill vapeSwill : allData) {
            boolean isContain = false;
            for (ItemIngredientInterface ingredient : ingredients) {
                if (Utils.getIngredientsFromList(vapeSwill.getAttributes()).contains( ingredient)) {
                    isContain = true;
                } else {
                    isContain = false;
                    break;
                }
            }
            if (isContain) {
                countIngredients++;
            }
        }
        return countIngredients;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sort_down:
                sort(currentData, true);
                break;
            case R.id.sort_up:
                sort(currentData, false);
                break;
        }
        mAdapter.notifyDataSetChanged();
    }

    private void sort(List<VapeSwill> data, boolean isSortDown) {
        Collections.sort(data, (vapeSwill, t1) -> {
            if (Integer.parseInt(vapeSwill.getPrice()) > Integer.parseInt(t1.getPrice()))
                return isSortDown ? -1 : 1;
            if (Integer.parseInt(vapeSwill.getPrice()) < Integer.parseInt(t1.getPrice()))
                return isSortDown ? 1 : -1;
            return 0;
        });
    }

    public void setActivityListener(MainActivityListener mActivityListener) {
        this.mActivityListener = mActivityListener;
    }

    public void setData(List<VapeSwill> allProductsData) {
        allData.clear();
        currentData.clear();
        allData.addAll(allProductsData);
        currentData.addAll(allProductsData);
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }
}
