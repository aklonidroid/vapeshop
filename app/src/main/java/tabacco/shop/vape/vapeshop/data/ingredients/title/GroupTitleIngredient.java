package tabacco.shop.vape.vapeshop.data.ingredients.title;

public class GroupTitleIngredient implements ItemIngredientInterface {
    public String title;

    public GroupTitleIngredient(String title) {
        this.title = title;
    }

    @Override
    public boolean isSection() {
        return true;
    }
}