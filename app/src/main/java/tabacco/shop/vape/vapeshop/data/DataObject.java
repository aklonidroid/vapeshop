package tabacco.shop.vape.vapeshop.data;

import java.io.Serializable;
import java.util.List;

public class DataObject implements Serializable {
    private List<Category> categories;
    private List<VapeSwill> products;
    private List<Manufacturer> manufacturer;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<VapeSwill> getProducts() {
        return products;
    }

    public void setProducts(List<VapeSwill> products) {
        this.products = products;
    }

    public List<Manufacturer> getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(List<Manufacturer> manufacturer) {
        this.manufacturer = manufacturer;
    }
}
