package tabacco.shop.vape.vapeshop.data.ingredients.title;

public interface ItemIngredientInterface {
    boolean isSection();
}
