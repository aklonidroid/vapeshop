package tabacco.shop.vape.vapeshop.view;

import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import tabacco.shop.vape.vapeshop.R;
import tabacco.shop.vape.vapeshop.adapter.MainFragmentAdapter;
import tabacco.shop.vape.vapeshop.data.Category;
import tabacco.shop.vape.vapeshop.listeners.MainActivityListener;

public class MainFragment extends BaseFragment implements View.OnClickListener {
    public static int ALL_ITEMS = -1;
    private MainActivityListener mActivityListener;
    private List<Category> mCategoriesList = new ArrayList<>();

    @Override
    public int getLayout() {
        return R.layout.fragment_main;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.all_products_button:
                mActivityListener.onCategoryButtonClick(ALL_ITEMS);
                break;
        }
    }

    @Override
    public void createContent(View view) {
        mRecyclerView = view.findViewById(R.id.main_recycler_view);
        Button allProductsButton = view.findViewById(R.id.all_products_button);

        allProductsButton.setOnClickListener(this);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
        mAdapter = new MainFragmentAdapter(mCategoriesList, R.layout.item_main_fragment_category);
        ((MainFragmentAdapter) mAdapter).setOnActivityLister(mActivityListener);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        clear();
    }

    private void clear() {
        if(mCategoriesList != null) {
            mCategoriesList.clear();
        }
        mRecyclerView = null;
        mAdapter = null;
    }

    public void setActivityListener(MainActivityListener activityListener) {
        this.mActivityListener = activityListener;
    }

    public void setDataCategories(List<Category> categoriesList) {
        this.mCategoriesList.clear();
        this.mCategoriesList.addAll(categoriesList);
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

}
