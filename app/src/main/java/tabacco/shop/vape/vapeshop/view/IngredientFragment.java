package tabacco.shop.vape.vapeshop.view;

import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tabacco.shop.vape.vapeshop.R;
import tabacco.shop.vape.vapeshop.adapter.IngredientsAdapter;
import tabacco.shop.vape.vapeshop.data.ingredients.Ingredient;
import tabacco.shop.vape.vapeshop.data.ingredients.title.ItemIngredientInterface;
import tabacco.shop.vape.vapeshop.listeners.IngredientsListener;
import tabacco.shop.vape.vapeshop.utils.Utils;

public class IngredientFragment extends BaseFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    public static final int SECTION_VIEW = 0;
    public static final int CONTENT_VIEW = 1;
    private ToggleButton mSelectedButton;
    private IngredientsListener mIngredientsListener;
    private List<ItemIngredientInterface> mSelectedFlavors = new ArrayList<>();
    private List<Ingredient> mAllIngredients = new ArrayList<>();
    private List<ItemIngredientInterface> mUsersAndSectionList = new ArrayList<>();

    @Override
    public int getLayout() {
        return R.layout.fragment_ingredients;
    }

    @Override
    public void createContent(View view) {
        Button mHideButton = view.findViewById(R.id.allow);
        Button mClearAllButton = view.findViewById(R.id.clear_all);
        mSelectedButton = view.findViewById(R.id.selected);
        mRecyclerView = view.findViewById(R.id.ingredients_recycler_view);

        mHideButton.setOnClickListener(this);
        mClearAllButton.setOnClickListener(this);
        mSelectedButton.setOnCheckedChangeListener(this);

        mRecyclerView.setHasFixedSize(true);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), Utils.isTablet(Objects.requireNonNull(getContext())) ? 10 : 6);
       //в строке 10 ингридиентов либо 1 заголовок категории
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (SECTION_VIEW == mAdapter.getItemViewType(position)) {
                    return 10;
                }
                return 1;
            }
        });

        getSectionalList(mAllIngredients);

        mRecyclerView.setLayoutManager(gridLayoutManager);
        mAdapter = new IngredientsAdapter(getContext(), R.layout.item_ingredients, mUsersAndSectionList, mSelectedFlavors);
        ((IngredientsAdapter) mAdapter).setOnIngredientsListener(mIngredientsListener);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.allow:
                ((MainActivity) Objects.requireNonNull(getActivity())).showVapeSwillBySelectedIngredients(mSelectedFlavors);
                break;
            case R.id.clear_all:
                clearSelectedData();
                break;
        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        clearSelectedData();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            ((IngredientsAdapter) mAdapter).showSelectedIngredientsOnly();
        } else {
            ((IngredientsAdapter) mAdapter).hideSelectedIngredientsOnly();
        }
    }

    private void getSectionalList(List<Ingredient> usersList) {
        mUsersAndSectionList.clear();
        Utils.sortAndAddSection(mUsersAndSectionList, usersList);
    }

    public void clearSelectedData() {
        //очищаем выбранные ингридиенты
        if(mSelectedFlavors != null) {
            for (ItemIngredientInterface ingredient : mSelectedFlavors) {
                ((Ingredient) ingredient).setSelected(false);
            }
            mSelectedFlavors.clear();
        }
        if(mSelectedFlavors != null) {
            mSelectedFlavors.clear();
        }
        if(mAdapter != null) {
            ((IngredientsAdapter)mAdapter).getSectionalList();
            mAdapter.notifyDataSetChanged();
        }
    }

    public void setIngredientsListener(IngredientsListener ingredientsListener) {
        this.mIngredientsListener = ingredientsListener;
    }

    public void setAllIngredients(List<Ingredient> allIngredients) {
        this.mAllIngredients.clear();
        this.mAllIngredients.addAll(allIngredients);
        getSectionalList(allIngredients);

        if (mAdapter != null) {
            ((IngredientsAdapter)mAdapter).getSectionalList();
            mAdapter.notifyDataSetChanged();
        }
    }

    //скролим до нужного вкуса
    public void scrollBySuggestion(SearchSuggestion searchSuggestion) {
        int index = ((IngredientsAdapter) mAdapter).getPositionOfIngredient(searchSuggestion);
        mRecyclerView.scrollToPosition(index);
    }

    public ToggleButton getSelectedButton() {
        return mSelectedButton;
    }

    public List<ItemIngredientInterface> setSelectFromDetail(Ingredient ingredient) {
        ingredient.setSelected(true);
        mSelectedFlavors.clear();
        mSelectedFlavors.add(ingredient);
        return mSelectedFlavors;
    }
}
