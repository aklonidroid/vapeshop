package tabacco.shop.vape.vapeshop.data;

public class Items {
    //вкусы
    public static final String ALCOHOL = "Алкогольные";
    public static final String DESSERT = "Десертные";
    public static final String TOBACCO = "Табачные";
    public static final String FRESH = "Освежающие";
    public static final String FRUIT = "Фруктовые";
    public static final String EXOTIC = "Экзотика";
    public static final String SOFT = "Soft Drinks";
    public static final String ALL_ITEMS = "All Items";

    //меню
    public static final String INGREDIENTS = "Инглириетны";
    public static final String NEW_ITEMS = "Новинки";
    public static final String STOCK = "Акции";
    public static final String MANUFACTURES = "Производители";


}
