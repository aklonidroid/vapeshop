package tabacco.shop.vape.vapeshop.listeners;


import tabacco.shop.vape.vapeshop.data.Manufacturer;
import tabacco.shop.vape.vapeshop.data.VapeSwill;

public interface MainActivityListener {
    void onCategoryButtonClick(int categoryId);
    void onVendorItemClick(Manufacturer vendor);
    void onDetailClick(VapeSwill item);
}
