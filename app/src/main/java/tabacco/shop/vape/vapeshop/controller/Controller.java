package tabacco.shop.vape.vapeshop.controller;

import tabacco.shop.vape.vapeshop.model.Model;

public class Controller {
    private Model model;

    public Controller(Model model) {
        if(model == null) {
            throw new IllegalArgumentException();
        }
        this.model = model;
    }

    public void onGetAllData(String url) {
        model.getAllData(url);
    }
}
