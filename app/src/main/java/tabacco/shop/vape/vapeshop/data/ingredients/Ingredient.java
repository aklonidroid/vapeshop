package tabacco.shop.vape.vapeshop.data.ingredients;

import android.support.annotation.NonNull;

import java.util.Objects;

import tabacco.shop.vape.vapeshop.data.ingredients.title.ItemIngredientInterface;

public class Ingredient implements Comparable<Ingredient>, ItemIngredientInterface {
    private String name;
    private String url;

    private boolean isSelected;

    @Override
    public int compareTo(@NonNull Ingredient ingredient) {
        return name.compareToIgnoreCase(ingredient.getName());
    }

    @Override
    public boolean isSection() {
        return false;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Ingredient)) return false;
        Ingredient flavor = (Ingredient) o;
        return name.equals(flavor.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
