package tabacco.shop.vape.vapeshop.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Parcel;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import tabacco.shop.vape.vapeshop.R;
import tabacco.shop.vape.vapeshop.data.Attribute;
import tabacco.shop.vape.vapeshop.data.DataObject;
import tabacco.shop.vape.vapeshop.data.VapeSwill;
import tabacco.shop.vape.vapeshop.data.ingredients.Booster;
import tabacco.shop.vape.vapeshop.data.ingredients.Desert;
import tabacco.shop.vape.vapeshop.data.ingredients.Drink;
import tabacco.shop.vape.vapeshop.data.ingredients.Fresh;
import tabacco.shop.vape.vapeshop.data.ingredients.Fruit;
import tabacco.shop.vape.vapeshop.data.ingredients.Ingredient;
import tabacco.shop.vape.vapeshop.data.ingredients.Tobacco;
import tabacco.shop.vape.vapeshop.data.ingredients.title.GroupTitleIngredient;
import tabacco.shop.vape.vapeshop.data.ingredients.title.ItemIngredientInterface;
import tabacco.shop.vape.vapeshop.data.other.NicotineContent;
import tabacco.shop.vape.vapeshop.data.other.Size;

public class Utils {

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void setStockPrice(TextView mTextView, String price, String oldPrice) {
        if (TextUtils.isEmpty(oldPrice)) {
            mTextView.setText(price + " грн");
            return;
        }
        ForegroundColorSpan redForegroundColorSpan = new ForegroundColorSpan(
                mTextView.getResources().getColor(android.R.color.black));

        SpannableStringBuilder ssb = new SpannableStringBuilder(price);
        ssb.append(" ")
                .append("грн");
        ssb.setSpan(redForegroundColorSpan, 0, ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssb.append(" ");

        StrikethroughSpan strikethroughSpan = new StrikethroughSpan();
        TextPaint textPaint = new TextPaint();
        textPaint.bgColor = mTextView.getResources().getColor(R.color.red);
        strikethroughSpan.getUnderlying().updateDrawState(textPaint);
        strikethroughSpan.updateDrawState(textPaint);

        ssb.append(oldPrice);
        ssb.setSpan(
                strikethroughSpan,
                ssb.length() - oldPrice.length(),
                ssb.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.parseColor("#ff0000")), ssb.length() - oldPrice.length(),
                ssb.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTextView.setText(ssb, TextView.BufferType.EDITABLE);
    }

    //Достаем все ингридиенты
    public static Set<Ingredient> getAllFlavors(DataObject dataObject) {
        Set<Ingredient> ingredients = new HashSet<>();
        for (VapeSwill vapeSwill : dataObject.getProducts()) {
            Attribute attribute = vapeSwill.getAttributes();
            if (attribute == null) {
                continue;
            }
            addIngredients(ingredients, attribute.getNicotineContents());
            addIngredients(ingredients, attribute.getSizes());
            addIngredients(ingredients, attribute.getTobacco());
            addIngredients(ingredients, attribute.getDrinks());
            addIngredients(ingredients, attribute.getBooster());
            addIngredients(ingredients, attribute.getDeserts());
            addIngredients(ingredients, attribute.getFruits());
            addIngredients(ingredients, attribute.getFreshes());
        }
        return ingredients;
    }

    private static void addIngredients(Set<Ingredient> ingredients, List<? extends Ingredient> sameIngredients) {
        if (sameIngredients != null) {
            ingredients.addAll(sameIngredients);
        }
    }


    public static List<Ingredient> sortIngredients(Set<Ingredient> currentIngredients) {
        List<Ingredient> all = new ArrayList<>();
        List<Ingredient> nicotine = new ArrayList<>();
        ArrayList<Ingredient> sizes = new ArrayList<>();
        ArrayList<Ingredient> tobacco = new ArrayList<>();
        ArrayList<Ingredient> fruits = new ArrayList<>();
        ArrayList<Ingredient> drinks = new ArrayList<>();
        ArrayList<Ingredient> deserts = new ArrayList<>();
        ArrayList<Ingredient> boosters = new ArrayList<>();
        ArrayList<Ingredient> fresh = new ArrayList<>();
        for (Ingredient ingredient : currentIngredients) {
            if (ingredient instanceof NicotineContent) {
                nicotine.add(ingredient);
            } else if (ingredient instanceof Size) {
                sizes.add(ingredient);
            } else if (ingredient instanceof Tobacco) {
                tobacco.add(ingredient);
            } else if (ingredient instanceof Fruit) {
                fruits.add(ingredient);
            } else if (ingredient instanceof Booster) {
                boosters.add(ingredient);
            } else if (ingredient instanceof Desert) {
                deserts.add(ingredient);
            } else if (ingredient instanceof Drink) {
                drinks.add(ingredient);
            } else if (ingredient instanceof Fresh) {
                fresh.add(ingredient);
            }
        }
        Collections.sort(nicotine);
        Collections.sort(sizes);
        Collections.sort(tobacco);
        Collections.sort(fruits);
        Collections.sort(drinks);
        Collections.sort(boosters);
        Collections.sort(deserts);
        Collections.sort(fresh);
        all.addAll(nicotine);
        all.addAll(sizes);
        all.addAll(tobacco);
        all.addAll(fruits);
        all.addAll(drinks);
        all.addAll(deserts);
        all.addAll(boosters);
        all.addAll(fresh);
        return all;
    }

    public static List<? extends SearchSuggestion> createSuggestion(Set<Ingredient> sortedIngredients, String newQuery) {
        List<SearchSuggestion> suggestions = new ArrayList<>();
        //сортировка по алфавиту
        Set<Ingredient> treeSet = new TreeSet<>(sortedIngredients);
        for (Ingredient ingredient : treeSet) {
            //сравнение не должно зависить от регистра
            if (ingredient.getName().toLowerCase().startsWith(newQuery.toLowerCase()))
                suggestions.add(new SearchSuggestion() {
                    @Override
                    public String getBody() {
                        return ingredient.getName();
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel parcel, int i) {

                    }
                });
        }

        return suggestions;
    }

    public static List<VapeSwill> getNewItems(DataObject dataObject) {
        List<VapeSwill> currentData = new ArrayList<>();
        Calendar c = new GregorianCalendar();
        c.setTimeInMillis(System.currentTimeMillis());
        c.add(Calendar.MONTH, -1);
        Date dateMonthAgo = c.getTime();
        for (VapeSwill vapeSwill : dataObject.getProducts()) {
            Date date = vapeSwill.getDate_added();
            if (date != null && date.after(dateMonthAgo)) {
                currentData.add(vapeSwill);
            }
        }
        return currentData;
    }

    public static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
        return (xlarge || large);
    }

    public static boolean isDeviceOnline(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    public static List<Ingredient> getIngredientsFromList(Attribute attribute) {
        List<Ingredient> ingredients = new ArrayList<>();
        if (attribute.getNicotineContents() != null) {
            ingredients.addAll(attribute.getNicotineContents());
        }
        if (attribute.getSizes() != null) {
            ingredients.addAll(attribute.getSizes());
        }
        ingredients.addAll(getAttributeList(attribute));
        return ingredients;
    }

    //Извлекаем вкусы(ингридиенты) из каждого item для отображения в подробностях
    public static List<Ingredient> getAttributeList(Attribute attribute) {
        //TODO если будут добавлятся вкусы, то нужно их добавлять тут и в методе getAllFlavors класса Utils
        List<Ingredient> allFlavors = new ArrayList<>();
        if (attribute.getDrinks() != null)
            allFlavors.addAll(attribute.getDrinks());
        if (attribute.getBooster() != null)
            allFlavors.addAll(attribute.getBooster());
        if (attribute.getDeserts() != null)
            allFlavors.addAll(attribute.getDeserts());
        if (attribute.getFruits() != null)
            allFlavors.addAll(attribute.getFruits());
        if (attribute.getTobacco() != null)
            allFlavors.addAll(attribute.getTobacco());
        if (attribute.getFreshes() != null)
            allFlavors.addAll(attribute.getFreshes());
        return allFlavors;
    }


    public static void sortAndAddSection(List<ItemIngredientInterface> sectionListToFill, List<Ingredient> ingredientList) {
        Collections.sort(ingredientList, (ingredient1, ingredient2) -> ingredient1.getName().compareTo(ingredient2.getName()) > 0 ? 1 : 0);

        String lastHeader = "";
        int size = ingredientList.size();

        for (int i = 0; i < size; i++) {
            Ingredient ingredient = ingredientList.get(i);
            String header = ingredient.getClass().getSimpleName();

            if (!TextUtils.equals(lastHeader, header)) {
                lastHeader = header;
                sectionListToFill.add(new GroupTitleIngredient(header));
            }
            sectionListToFill.add(ingredient);
        }
    }
}
