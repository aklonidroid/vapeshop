package tabacco.shop.vape.vapeshop.view;

import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import tabacco.shop.vape.vapeshop.R;
import tabacco.shop.vape.vapeshop.adapter.VendorAdapter;
import tabacco.shop.vape.vapeshop.data.Manufacturer;
import tabacco.shop.vape.vapeshop.listeners.MainActivityListener;
import tabacco.shop.vape.vapeshop.utils.Utils;

public class VendorFragment extends BaseFragment {
    private static List<Manufacturer> vendorData = new ArrayList<>();
    private MainActivityListener mActivityListener;

    @Override
    public int getLayout() {
        return R.layout.fragment_manufactures;
    }

    @Override
    public void createContent(View view) {
        mRecyclerView = view.findViewById(R.id.manufactures_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), Utils.isTablet(Objects.requireNonNull(getContext())) ? 5 : 3));
        mAdapter = new VendorAdapter(vendorData, R.layout.item_vendor);
        ((VendorAdapter) mAdapter).setOnActivityLister(mActivityListener);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        vendorData.clear();
        mAdapter = null;
        mRecyclerView = null;
    }

    public void setVendors(List<Manufacturer> manufactures) {
        vendorData.clear();
        vendorData.addAll(manufactures);
    }

    public void setActivityListener(MainActivityListener activityListener) {
        this.mActivityListener = activityListener;
    }
}
