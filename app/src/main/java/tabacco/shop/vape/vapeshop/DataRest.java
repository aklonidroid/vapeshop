package tabacco.shop.vape.vapeshop;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;
import tabacco.shop.vape.vapeshop.data.DataObject;


public interface DataRest {
    @GET
    Call<DataObject> getAllData(@Url String url);
}
