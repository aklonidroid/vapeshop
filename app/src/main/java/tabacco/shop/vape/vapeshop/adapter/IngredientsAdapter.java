package tabacco.shop.vape.vapeshop.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import tabacco.shop.vape.vapeshop.R;
import tabacco.shop.vape.vapeshop.data.VapeSwill;
import tabacco.shop.vape.vapeshop.data.ingredients.Ingredient;
import tabacco.shop.vape.vapeshop.data.ingredients.title.GroupTitleIngredient;
import tabacco.shop.vape.vapeshop.data.ingredients.title.ItemIngredientInterface;
import tabacco.shop.vape.vapeshop.listeners.IngredientsListener;
import tabacco.shop.vape.vapeshop.utils.Utils;

import static tabacco.shop.vape.vapeshop.utils.Utils.getIngredientsFromList;
import static tabacco.shop.vape.vapeshop.utils.Utils.sortIngredients;
import static tabacco.shop.vape.vapeshop.view.CategoryFragment.allData;
import static tabacco.shop.vape.vapeshop.view.IngredientFragment.CONTENT_VIEW;
import static tabacco.shop.vape.vapeshop.view.IngredientFragment.SECTION_VIEW;

public class IngredientsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int mIngredientsItemLayout;
    private List<ItemIngredientInterface> mIngredientsAndSectionList;
    private List<ItemIngredientInterface> mIngredientsAll;
    private List<ItemIngredientInterface> mSelectedList;
    private IngredientsListener mIngredientsListener;
    private boolean showSelected;
    private Context context;


    public IngredientsAdapter(Context context, int mIngredientsItemLayout, List<ItemIngredientInterface> mIngredientsList, List<ItemIngredientInterface> mSelectedList) {
        this.context = context;
        this.mIngredientsAndSectionList = new ArrayList<>(mIngredientsList);
        this.mIngredientsAll = mIngredientsList;
        this.mIngredientsItemLayout = mIngredientsItemLayout;
        this.mSelectedList = mSelectedList;
        getSectionalList();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == SECTION_VIEW) {
            return new SectionViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_group_title, parent, false));
        }
        return new IngredientsHolder(LayoutInflater.from(parent.getContext()).inflate(mIngredientsItemLayout, parent, false));
    }

    @Override
    public int getItemCount() {
        if (showSelected) {
            return mSelectedList.size();
        }
        return mIngredientsAndSectionList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holderGeneral, @SuppressLint("RecyclerView") final int position) {
        //Элемент названия категории атрибутов
        if (SECTION_VIEW == getItemViewType(position)) {
            SectionViewHolder sectionViewHolder = (SectionViewHolder) holderGeneral;
            GroupTitleIngredient sectionItem = ((GroupTitleIngredient) mIngredientsAndSectionList.get(position));
            sectionViewHolder.title.setText(sectionItem.title);
            return;
        }

        IngredientsHolder ingredientsHolder = (IngredientsHolder) holderGeneral;
        final Ingredient ingredient;

        if (showSelected) {
            ingredient = (Ingredient) mSelectedList.get(position);
        } else {
            ingredient = (Ingredient) mIngredientsAndSectionList.get(position);
        }

        ingredientsHolder.mTitle.setText(ingredient.getName());

        Glide.with(context)
                .load((ingredient).getUrl())
                .into(ingredientsHolder.mImageIngredient);

        if (showSelected) {
            ingredientsHolder.mCountTextView.setText("-");
            ingredientsHolder.mRedCircle.setBackground(context.getResources().getDrawable(R.drawable.circle_red));
            ingredientsHolder.mIngredientsLinearContainer.setBackground(context.getResources().getDrawable(R.drawable.listview_border));
            ingredientsHolder.mIngredientsLinearContainer.setVisibility(View.VISIBLE);
        } else {
            if (ingredient.isSelected()) {
                ingredientsHolder.mCountTextView.setText("" + mIngredientsListener.getCountByIngredients(mSelectedList));
                ingredientsHolder.mRedCircle.setBackground(context.getResources().getDrawable(R.drawable.circle_red));
                ingredientsHolder.mIngredientsLinearContainer.setBackground(context.getResources().getDrawable(R.drawable.listview_border));
                ingredientsHolder.mIngredientsLinearContainer.setVisibility(View.VISIBLE);
            } else {
                ingredientsHolder.mCountTextView.setText("");
                ingredientsHolder.mRedCircle.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                ingredientsHolder.mIngredientsLinearContainer.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                ingredientsHolder.mIngredientsLinearContainer.setVisibility(View.VISIBLE);
            }
        }

        ingredientsHolder.mIngredientsLinearContainer.setOnClickListener(view -> {
            if (ingredient.isSelected()) {
                view.setBackgroundColor(context.getResources().getColor(R.color.transparent));
                ingredient.setSelected(false);
                mSelectedList.remove(ingredient);
            } else {
                view.setBackground(context.getResources().getDrawable(R.drawable.listview_border));
                ingredient.setSelected(true);
                mSelectedList.add((ingredient));
            }
            getSectionalList();
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemViewType(int position) {
        if (showSelected) {
            return getView(mSelectedList, position);
        } else {
            return getView(mIngredientsAndSectionList, position);
        }
    }

    //после выбора элемента, нужно оставить только те вкусы, которые содержатся вместе с выделенными в жиже
    public void getSectionalList() {
        mIngredientsAndSectionList.clear();
        List<Ingredient> ingredientList = getContainsList(mSelectedList);
        if (ingredientList == null) {
            mIngredientsAndSectionList.addAll(mIngredientsAll);
            return;
        }
        Utils.sortAndAddSection(mIngredientsAndSectionList, ingredientList);

    }


    //возвращаем список вкусов которые так же содержатся в тех же товарах что уже выбраны
    private List<Ingredient> getContainsList(List<ItemIngredientInterface> selectedList) {
        if (selectedList.size() == 0) {
            return null;
        }
        Set<Ingredient> set = new HashSet<>();
        boolean isContain;
        for (VapeSwill vapeSwill : allData) {
            List<Ingredient> ingredients = getIngredientsFromList(vapeSwill.getAttributes());
            isContain = true;
            for (ItemIngredientInterface ingredientAdapter : selectedList) {
                if (ingredientAdapter instanceof Ingredient && !ingredients.contains(ingredientAdapter)) {
                    isContain = false;
                }
            }
            if (isContain) {
                for (Ingredient ingr : ingredients) {
                    for (ItemIngredientInterface ingre : mIngredientsAll) {
                        if (ingre instanceof Ingredient && ingr.equals(ingre)) {
                            set.add((Ingredient) ingre);
                        }
                    }
                }

            }
        }

        return sortIngredients(set);
    }

    private int getView(List<ItemIngredientInterface> list, int position) {
        if (list.get(position).isSection()) {
            return SECTION_VIEW;
        } else {
            return CONTENT_VIEW;
        }
    }

    public void setOnIngredientsListener(IngredientsListener ingredientListener) {
        this.mIngredientsListener = ingredientListener;
    }

    //возвращает позицию элемента в списке
    public int getPositionOfIngredient(SearchSuggestion searchSuggestion) {
        for (int position = 0; position < mIngredientsAndSectionList.size(); position++) {
            if (SECTION_VIEW == getItemViewType(position)) {
                continue;
            }
            if (((Ingredient) mIngredientsAndSectionList.get(position)).getName().equals(searchSuggestion.getBody())) {
                return mIngredientsAndSectionList.indexOf(mIngredientsAndSectionList.get(position));
            }
        }
        return -1;
    }

    public void showSelectedIngredientsOnly() {
        showSelected = true;
        notifyDataSetChanged();
    }

    public void hideSelectedIngredientsOnly() {
        showSelected = false;
        notifyDataSetChanged();
    }

    static class IngredientsHolder extends RecyclerView.ViewHolder {
        private TextView mTitle;
        private ImageView mImageIngredient;
        private TextView mCountTextView;
        private LinearLayout mIngredientsLinearContainer;
        private FrameLayout mRedCircle;

        IngredientsHolder(View view) {
            super(view);
            mTitle = view.findViewById(R.id.title_ingredient);
            mImageIngredient = view.findViewById(R.id.image_ingredient);
            mCountTextView = view.findViewById(R.id.view_alert_count_textview);
            mRedCircle = view.findViewById(R.id.view_alert_red_circle);
            mIngredientsLinearContainer = view.findViewById(R.id.ingredients_linear_container);
        }
    }

     class SectionViewHolder extends RecyclerView.ViewHolder {
        TextView title;

        SectionViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tv_group_title);
        }
    }
}
