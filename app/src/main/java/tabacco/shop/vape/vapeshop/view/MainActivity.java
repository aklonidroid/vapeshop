package tabacco.shop.vape.vapeshop.view;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import tabacco.shop.vape.vapeshop.R;
import tabacco.shop.vape.vapeshop.controller.Controller;
import tabacco.shop.vape.vapeshop.data.Category;
import tabacco.shop.vape.vapeshop.data.DataObject;
import tabacco.shop.vape.vapeshop.data.Manufacturer;
import tabacco.shop.vape.vapeshop.data.VapeSwill;
import tabacco.shop.vape.vapeshop.data.ingredients.Ingredient;
import tabacco.shop.vape.vapeshop.data.ingredients.title.ItemIngredientInterface;
import tabacco.shop.vape.vapeshop.listeners.CallbackView;
import tabacco.shop.vape.vapeshop.listeners.IngredientsListener;
import tabacco.shop.vape.vapeshop.listeners.MainActivityListener;
import tabacco.shop.vape.vapeshop.model.Model;
import tabacco.shop.vape.vapeshop.utils.Utils;

import static tabacco.shop.vape.vapeshop.utils.Utils.getAttributeList;
import static tabacco.shop.vape.vapeshop.view.MainFragment.ALL_ITEMS;

public class MainActivity extends AppCompatActivity implements CallbackView, MainActivityListener, View.OnClickListener {
    //TODO кнопки сортировки добавить значки
    //TODO кнопкам на главную и назад добавить селектор
    //TODO поиск исправить view, что бы красиво отобраалось
    private static final String PREFERENCES = "PREFERENCES";
    private static final String LINK = "LINK";
    private static final String DATA = "DATA";
    private static final String DETAIL = "DETAIL";

    private static Set<Ingredient> allIngredients = new HashSet<>();
    private static Set<Ingredient> currentIngredients = new HashSet<>();
    private static String baseLink;
    private BroadcastReceiver broadcastReceiver;
    private PendingIntent pendingIntent;
    private List<Category> categories = new ArrayList<>();
    private DataObject dataObject;
    private RelativeLayout mProgressBarContainer;
    private FrameLayout mIngredientsFragmentContainer;
    private MainFragment mMainFragment;
    private CategoryFragment mCategoryFragment;
    private VendorFragment mVendorFragment;
    private DetailFragment mDetailFragmentCurrent;
    private IngredientFragment mIngredientsFragment;
    private FloatingSearchView mSearchView;
    //текущий фрагмент, который слушает результаты поиска
    private IngredientsListener mIngredientsListener;
    private Button mIngredientsButton;
    private AlarmManager alarmManager;
    private Controller controller;
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private SharedPreferences mSharedPreference;
    //счетчик нажатий для обновления
    private int countToRefresh = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        initUI();
        initializeLink();
        setAlarmManagerAndReceiver();

        if (Utils.isDeviceOnline(this) && baseLink != null) {
            createControllerAndRefresh(baseLink, true);
        } else {
            String data = mSharedPreference.getString(DATA, null);
            if (data != null) {
                Gson gson = new Gson();
                DataObject dataObject = gson.fromJson(data, DataObject.class);
                update(dataObject);
            } else {
                if(baseLink != null) {
                    Toast.makeText(this, "Введите ссылку", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.no_connection), Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(broadcastReceiver, new IntentFilter(getPackageName()));

        pendingIntent = PendingIntent.getBroadcast(this, 0, new Intent(getPackageName()), 0);
        //обновление каждые 3 часа
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME,
//                SystemClock.elapsedRealtime() + 60000, 10800000, pendingIntent);
                SystemClock.elapsedRealtime() + 6000000, 1000, pendingIntent);
    }

    @Override
    protected void onStop() {
        super.onStop();
        alarmManager.cancel(pendingIntent);
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        //возвращаемся в главный экран
        if (mDetailFragmentCurrent != null && mDetailFragmentCurrent.isVisible()) {
            fragmentManager.beginTransaction().hide(mDetailFragmentCurrent).commit();
            showIngredientsButton();
        } else if (mCategoryFragment != null && mCategoryFragment.isVisible()
                || mVendorFragment != null && mVendorFragment.isVisible()) {
            attachMainFragment();
        }
        //Не выходить из приложения при нажатии кнопки назад
    }

    @Override
    public void updateAllData(DataObject dataObject) {
        Gson gson = new Gson();
        String json = gson.toJson(dataObject);
        SharedPreferences.Editor editor = mSharedPreference.edit();
        editor.putString(DATA, json);
        editor.apply();

        update(dataObject);
    }

    @Override
    public void onFailure() {
        stopLoading();
        Toast.makeText(this, getResources().getString(R.string.data_did_not_refresh), Toast.LENGTH_LONG).show();
    }

    //Callback по нажатию на производителя
    @Override
    public void onVendorItemClick(Manufacturer vendor) {
        currentIngredients.clear();
        List<VapeSwill> vapeSwills = new ArrayList<>();
        for (VapeSwill vapeSwill : dataObject.getProducts()) {
            if (vendor.getMan_name().equals(vapeSwill.getVendor())) {
                vapeSwills.add(vapeSwill);
                setCurrentIngredients(vapeSwill);
            }
        }
        attachCategoriesFragment(vapeSwills);
    }

    //Подробная игформация о товаре
    @Override
    public void onDetailClick(VapeSwill item) {
        mIngredientsButton.setVisibility(View.INVISIBLE);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        mDetailFragmentCurrent = (DetailFragment) getSupportFragmentManager().findFragmentByTag(DETAIL);
        if (mDetailFragmentCurrent == null) {
            mDetailFragmentCurrent = new DetailFragment();
        }
        mDetailFragmentCurrent.setVapeSwillData(item);

        if (mDetailFragmentCurrent.isAdded()) {
            transaction.show(mDetailFragmentCurrent);
            transaction.commit();
            return;
        }

        transaction.add(R.id.fragment_container, mDetailFragmentCurrent, DETAIL);
        transaction.commit();
    }

    public void setIngredientFromDetailFragment(Ingredient ingredient) {
        if (mIngredientsFragment == null) {
            mIngredientsFragment = new IngredientFragment();
        }
        onCategoryButtonClick(ALL_ITEMS);
        for (Ingredient ingrdCurrent : currentIngredients) {
            if (ingrdCurrent.getName().equals(ingredient.getName())) {
                ingredient = ingrdCurrent;
                ingrdCurrent.setSelected(true);
                break;
            }
        }
        //Сразу отобразить выделенные
        showVapeSwillBySelectedIngredients(mIngredientsFragment.setSelectFromDetail(ingredient));

    }

    @Override
    public void onCategoryButtonClick(int categoryId) {
        List<VapeSwill> vapeSwills = new ArrayList<>();
        List<VapeSwill> allData = dataObject.getProducts();
        currentIngredients.clear();
        if (categoryId == ALL_ITEMS) {
            currentIngredients.addAll(allIngredients);
            vapeSwills.addAll(allData);
        } else {
            for (VapeSwill vapeSwill : allData) {
                if (categoryId == vapeSwill.getCategoryId()) {
                    vapeSwills.add(vapeSwill);
                    setCurrentIngredients(vapeSwill);
                }
            }
        }
        attachCategoriesFragment(vapeSwills);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ingredients:
                showIngredientsSearchFragment();
                break;
            case R.id.new_items:
                currentIngredients.clear();
                List<VapeSwill> currentData = Utils.getNewItems(dataObject);
                for (VapeSwill vapeSwill : currentData) {
                    setCurrentIngredients(vapeSwill);
                }
                attachCategoriesFragment(currentData);
                break;
            case R.id.stock:
                List<VapeSwill> stockData = new ArrayList<>();
                for (VapeSwill vapeSwill : dataObject.getProducts()) {
                    if (!TextUtils.isEmpty(vapeSwill.getOldprice())) {
                        stockData.add(vapeSwill);
                    }
                }
                currentIngredients.clear();
                for (VapeSwill vapeSwill : stockData) {
                    setCurrentIngredients(vapeSwill);
                }
                attachCategoriesFragment(stockData);
                break;
            case R.id.manufactures:
                attachVendorFragment();
                break;
            case R.id.vape_menu_home:
                showDialogHome();
                break;
            case R.id.back_button:
                onBackPressed();
                break;
            case R.id.to_home:
                attachMainFragment();
                break;
            case R.id.button_add:
                baseLink = ((EditText) findViewById(R.id.edit_link)).getText().toString();
                findViewById(R.id.link_container).setVisibility(View.GONE);
                Controller controller = new Controller(new Model(this));
                controller.onGetAllData(baseLink);
                SharedPreferences.Editor editor = mSharedPreference.edit();
                editor.putString(LINK, baseLink);
                editor.apply();
                createControllerAndRefresh(baseLink, true);
                break;
        }
    }

    private void initUI() {
        Button newItemsButton = findViewById(R.id.new_items);
        Button stockButton = findViewById(R.id.stock);
        Button vendorButton = findViewById(R.id.manufactures);
        mIngredientsButton = findViewById(R.id.ingredients);
        mIngredientsFragmentContainer = findViewById(R.id.ingredients_frag_container);
        mProgressBarContainer = findViewById(R.id.progress_bar_container);

        mIngredientsButton.setOnClickListener(this);
        newItemsButton.setOnClickListener(this);
        stockButton.setOnClickListener(this);
        vendorButton.setOnClickListener(this);
        findViewById(R.id.vape_menu_home).setOnClickListener(this);
        findViewById(R.id.to_home).setOnClickListener(this);
        findViewById(R.id.back_button).setOnClickListener(this);
        findViewById(R.id.vape_menu_home).setOnLongClickListener(view -> {
            handsRefresh();
            return true;
        });

        initSearchView();
        attachMainFragment();
    }

    private void initializeLink() {
        mSharedPreference = getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        baseLink = mSharedPreference.getString(LINK, null);
        if (baseLink != null) {
            findViewById(R.id.link_container).setVisibility(View.GONE);
        } else {
            findViewById(R.id.button_add).setOnClickListener(this);
        }
    }

    private void setAlarmManagerAndReceiver() {
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                createControllerAndRefresh(baseLink, false);
            }
        };

    }

    private void createControllerAndRefresh(String link, boolean showLoading) {
        if (!Utils.isDeviceOnline(this) || TextUtils.isEmpty(baseLink)) {
            return;
        }
        if (showLoading) {
            startLoading();
        }
        if (controller == null) {
            controller = new Controller(new Model(this));
        }
        controller.onGetAllData(link);
    }

    private void update(DataObject dataObject) {
        this.dataObject = dataObject;

        Iterator<VapeSwill> iterator = dataObject.getProducts().iterator();
        while (iterator.hasNext()) {
            VapeSwill vapeSwill = iterator.next();
            if (vapeSwill.getAttributes() == null || !vapeSwill.getAvailable()) {
                iterator.remove();
            }
        }
        categories.clear();
        categories.addAll(dataObject.getCategories());
        mMainFragment.setDataCategories(categories);
        allIngredients.addAll(Utils.getAllFlavors(dataObject));
        if (mMainFragment != null && !mMainFragment.isVisible()) {
            attachMainFragment();
        }
        stopLoading();
    }

    public void showIngredientsButton() {
        mIngredientsButton.setVisibility(View.VISIBLE);
    }

    private void attachMainFragment() {
        mIngredientsButton.setVisibility(View.INVISIBLE);
        setVisibilityIngredientFragment(false);
        if (mMainFragment == null) {
            mMainFragment = new MainFragment();
        }
        mMainFragment.setActivityListener(this);
        mMainFragment.setDataCategories(categories);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, mMainFragment);
        transaction.commitAllowingStateLoss();
    }

    private void stopLoading() {
        mProgressBarContainer.setVisibility(View.GONE);
    }

    private void setCurrentIngredients(VapeSwill vapeSwill) {
        currentIngredients.addAll(getAttributeList(vapeSwill.getAttributes()));
    }

    private void attachCategoriesFragment(List<VapeSwill> data) {
        startLoading();
        if (mCategoryFragment == null) {
            mCategoryFragment = new CategoryFragment();
        }
        if (mIngredientsFragment != null) {
            mIngredientsFragment.clearSelectedData();
        }
        mIngredientsListener = mCategoryFragment;
        mCategoryFragment.setActivityListener(this);
        mCategoryFragment.setData(data);

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, mCategoryFragment);
        transaction.commit();
        showIngredientsButton();
        stopLoading();
    }


    private void showIngredientsSearchFragment() {
        setVisibilityIngredientFragment(true);
        if (mIngredientsFragment == null) {
            mIngredientsFragment = new IngredientFragment();
        }
        mIngredientsFragment.setIngredientsListener(mIngredientsListener);
        mIngredientsFragment.setAllIngredients(Utils.sortIngredients(currentIngredients));
        if (!mIngredientsFragment.isAdded()) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(R.id.ingredients_frag_container, mIngredientsFragment);
            transaction.commit();
        }
    }

    private void attachVendorFragment() {
        if (mVendorFragment == null) {
            mVendorFragment = new VendorFragment();
        }
        mVendorFragment.setVendors(dataObject.getManufacturer());
        mVendorFragment.setActivityListener(this);
        mIngredientsButton.setVisibility(View.INVISIBLE);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container, mVendorFragment);
        transaction.commit();
    }

    private void showDialogHome() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getResources().getString(R.string.back_to_home))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.yes), (dialogInterface, i) -> attachMainFragment())
                .setNegativeButton(getResources().getString(R.string.no),
                        (dialog, id) -> dialog.cancel());
        AlertDialog alert = builder.create();
        alert.show();
    }

    //обновляем данные если было нажато на логотип больше 3 раз
    private void handsRefresh() {
        if (countToRefresh >= 2) {
            Toast toast = Toast.makeText(MainActivity.this, getResources().getString(R.string.update_start), Toast.LENGTH_LONG);
            View toastView = toast.getView();
            TextView text = toastView.findViewById(android.R.id.message);
            text.setBackgroundColor(getResources().getColor(R.color.black));
            toastView.getBackground().setColorFilter(getResources().getColor(R.color.black), PorterDuff.Mode.SRC_IN);
            toast.show();
            countToRefresh = 0;
            createControllerAndRefresh(baseLink, true);
        }
        countToRefresh++;
    }

    private void initSearchView() {
        mSearchView = findViewById(R.id.floating_search_view);
        mSearchView.setOnQueryChangeListener((oldQuery, newQuery) -> {
            if (TextUtils.isEmpty(newQuery)) {
                mSearchView.clearSuggestions();
                return;
            }
            mSearchView.swapSuggestions(Utils.createSuggestion(currentIngredients, newQuery));
        });
        mSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
                mIngredientsFragment.getSelectedButton().setChecked(false);
                mIngredientsFragment.scrollBySuggestion(searchSuggestion);
                mSearchView.clearSuggestions();
                mSearchView.clearQuery();
                mSearchView.clearSearchFocus();
                Utils.hideKeyboard(MainActivity.this);
            }

            @Override
            public void onSearchAction(String currentQuery) {
            }
        });
        mSearchView.setOnBindSuggestionCallback((suggestionView, leftIcon, textView, item, itemPosition) -> {
            suggestionView.setBackgroundColor(getResources().getColor(R.color.white));
            leftIcon.setBackgroundColor(getResources().getColor(R.color.white));
            textView.setBackgroundColor(getResources().getColor(R.color.white));
        });
    }

    public void showVapeSwillBySelectedIngredients(List<ItemIngredientInterface> selectedFlavors) {
        setVisibilityIngredientFragment(false);
        mIngredientsListener.onIngredientsClick(selectedFlavors);
    }

    private void setVisibilityIngredientFragment(boolean isVisible) {
        mSearchView.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        mIngredientsFragmentContainer.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    private void startLoading() {
        mProgressBarContainer.setVisibility(View.VISIBLE);
    }
}
