package tabacco.shop.vape.vapeshop.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import tabacco.shop.vape.vapeshop.R;
import tabacco.shop.vape.vapeshop.data.Manufacturer;
import tabacco.shop.vape.vapeshop.listeners.MainActivityListener;

public class VendorAdapter extends RecyclerView.Adapter<VendorAdapter.MyViewHolder> {
    private List<Manufacturer> mVendorList;
    private MainActivityListener mActivityListener;
    private final int mItemLayout;

    public VendorAdapter(List<Manufacturer> mVendorList, int mItemLayout) {
        this.mVendorList = mVendorList;
        this.mItemLayout = mItemLayout;
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(mItemLayout, null, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        Manufacturer vendor = mVendorList.get(position);
        holder.mTextViewTitle.setText(vendor.getMan_name());
        Glide.with(holder.mImageView)
                .load(vendor.getMan_image())
                .into(holder.mImageView);
        holder.mContainer.setOnClickListener(view -> {
            if(mActivityListener != null) {
                mActivityListener.onVendorItemClick(vendor);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mVendorList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView mTextViewTitle;
        ImageView mImageView;
        LinearLayout mContainer;
        MyViewHolder(View v) {
            super(v);
            mTextViewTitle = v.findViewById(R.id.title);
            mImageView = v.findViewById(R.id.image_vape_swill);
            mContainer = v.findViewById(R.id.vendor_item_container);
        }
    }

    public void setOnActivityLister(MainActivityListener activityListener) {
        this.mActivityListener = activityListener;
    }

}
