package tabacco.shop.vape.vapeshop.listeners;

import tabacco.shop.vape.vapeshop.data.DataObject;

public interface CallbackView {
    void updateAllData(DataObject dataObject);
    void onFailure();
}
